const sdpConstraints = {offerToReceiveAudio: true, offerToReceiveVideo: true};

class WebRtcConnection {
  constructor(peerId,iceServers) {
    this.pc = null;
    this.pendingCandidates = [];
    this.remoteStream = null;
    this.iceServers = iceServers;
    this.peerId = peerId; //remote peer id, just makes logging easier, we could also just dispatch events for those things and let the caller log
    this.remoteDescriptionSet = false;
    this.callbacks = [];
  }
  processOffer(offer) {
    this.createPeerConnection(); //just let the exception happen - this is just a scaffold
    return new Promise((resolve, reject) => {
      return this.pc.setRemoteDescription(new RTCSessionDescription(offer))
      .then(() => {
        console.log('offer processed');
        this.remoteDescriptionSet = true;
        return this.pc.createAnswer()
        .then((answer) => {
          return this.pc.setLocalDescription(answer, () => {
            resolve(answer);
            //process any ice candidates that beat us processing  the offer/answer
            this.processPendingCandidates();
          })
        })
      })
      .catch((error) => {
        console.error('error processing offer', error);
        reject(error);
      });
    });
  }
  createOffer() {
    this.createPeerConnection();
    return new Promise((resolve, reject) => {
      this.pc.createOffer(sdpConstraints)
      .then((offer) => {
        this.pc.setLocalDescription(new RTCSessionDescription(offer))
        .then(() => {
          resolve(offer);
        });
      })
      .catch((error) => {
        console.log('error creating offer', error);
        reject(error);
      });
    });
  }
  createPeerConnection() {
    if(this.pc) {
      console.debug('closing peer connection for peer ' + this.peerId + ' because we are creating a new one');
      //since we're not using this pc anymore we dont' care about the listeners
      this.pc.onicecandidate = null;
      this.pc.onaddstream = null;
      this.pc.onremotestream = null;
      this.pc.oniceconnectionstatechange = null;
      try {
        this.pc.close();
      } catch(e) {
        //might not have been open?
        console.warn('could not close peer connection for peer ' + this.peerId, e);
      }
    }

    this.pc = new RTCPeerConnection({iceServers: this.iceServers});
    this.pc.onicecandidate = this.onIceCandidate.bind(this);
    this.pc.onaddstream = this.onRemoteStreamAdded.bind(this);
    this.pc.onremovestream = this.onRemoteStreamRemoved.bind(this);
    this.pc.oniceconnectionstatechange = this.onIceConnectionStateChange.bind(this);
    this.remoteDescriptionSet = false;

    if(this.localStream) {
        console.debug('Adding local stream to PeerConnection.');
        this.pc.addStream(this.localStream);
    }
  }
  setLocalStream(stream) {
    this.localStream = stream;
  }
  onIceCandidate(event) {
    if (event.candidate) {
      this.dispatch('candidate', event.candidate);
    } else {
      console.debug('End of candidates.');
    }
  }
  processCandidate(candidate) {
    if(!this.remoteDescriptionSet) {
      console.warn('received ice candidate prior to setting remote description');
      this.pendingCandidates.push(candidate);
      return;
    }
    this.addRemoteIceCandidate(candidate);
  }
  addRemoteIceCandidate(candidate) {
    console.log('add candidate', candidate);
    this.pc.addIceCandidate(new RTCIceCandidate(candidate)); //TODO do we have to pull out sdpmlineindex, etc like we do in LEA
  }
  processPendingCandidates() {
    var length = this.pendingCandidates.length;
    if(length > 0) {
      //warning here because this rarely seen and it would be good to know if there's a pattern
      //warns make it to console.warn
      console.warn('processing pending candidates for connection ' + this.peerId + ', length = ' + length);
      for(var i = 0; i < length; i++) {
        console.debug('Processing cached candidate for connection ' + this.peerId + ' ' + i);
        this.processCandidate(this.pendingCandidates[i]);
      }
      this.pendingCandidates = [];
    }
  }
  processAnswer(answer) {
    //returning a promise here just to help w/ debugging
    return new Promise((resolve, reject) => {
      this.pc.setRemoteDescription(new RTCSessionDescription(answer))
      .then(() => {
        this.remoteDescriptionSet = true;
        console.debug('processed answer for connection ' + this.peerId, answer);
        this.processPendingCandidates();
        resolve();
      })
      .catch((error) => {
        console.error('process answer error', error);
        reject(error);
      });
    });

  }
  onRemoteStreamAdded(event) {
    console.log('stream added for connection: ' + this.peerId, event);
    this.dispatch('streamAdded', event.stream);
  }
  onRemoteStreamRemoved() {
    console.warn('remote stream removed');
  }
  onIceConnectionStateChange() {
    const iceConnectionState = event.currentTarget.iceConnectionState;
    //see https://developer.mozilla.org/en-US/docs/Web/API/RTCPeerConnection/iceConnectionState
    //states are "new", "checking", "connected", "completed", "failed", "disconnected", and "closed"
    console.debug('ice connection state change for peerId ' + this.peerId + ': ' + iceConnectionState);
    this.dispatch('iceConnectionStateChange', iceConnectionState);
    this.iceConnectionState = iceConnectionState;
  }

  //very basic/simple eventing
  dispatch(type, payload) {
    this.callbacks[type](payload);
  }
  on(type, fn) {
    if(this.callbacks[type]) {
      console.warn('replacing a callback for type: ', type);
    }
    //only 1 callback per message type
    this.callbacks[type] = fn;
  }
  close() {
    if(this.pc) {
      this.pc.close();
    } else {
      console.warn('tried to close a pc that does not exist');
    }
  }
}
export default WebRtcConnection;
