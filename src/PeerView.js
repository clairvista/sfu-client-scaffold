import React, {Component, PropTypes} from 'react';

class PeerView extends Component {
  render() {
    return (
      <div style={{margin: '0 10px'}}>
        <video
          width='100%' height='auto' style={{backgroundColor: 'black'}}
          ref={(videoEl) => { this.videoEl = videoEl}}
          autoPlay
        />
        <br/>
        <span>{this.getSpanText()}</span><br/>
        <span>Connection ID: {this.props.connectionId}</span><br/>
        <span>Client ID: {this.props.clientId}</span>
      </div>
    );
  }
  setSrcObject(stream) {
    this.videoEl.srcObject = stream;
  }
  getSpanText() {
    const {isLocal, connectionState} = this.props;
    if(isLocal) {
      return 'Upstream Connection State: ' + connectionState
    } else {
      return 'Downstream Connection State: ' + connectionState
    }
  }
}

PeerView.propTypes = {
  connectionId: PropTypes.string.isRequired,
  clientId: PropTypes.string.isRequired,
  connectionState: PropTypes.string,
  isLocal: PropTypes.bool
};
export default PeerView;
