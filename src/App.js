import React, { Component } from 'react';
import './App.css';
import '../node_modules/stompjs/lib/stomp';
import '../node_modules/webrtc-adapter/src/js/adapter_core';
import WebRtcConnection from './WebRtcConnection';
import PeerConnector from './PeerConnector';
import PeerView from './PeerView';
import uuidV4 from 'uuid/v4';
import {Map} from 'immutable';

const applicationId = 'chat.liveexpert';

class App extends Component {
  constructor(props) {
    super(props);
    //TODO see if there is a syntax to define a variable as a key on a map in a single pass
    let initPeers = {};
    //initPeers[sendId] = {iceConnectionState: 'no connection yet', clientId: 'not known yet'};
    this.state = {
      peers: Map(initPeers) //maintain an (immutable) map of connectionIds on state, so the whole webRtcConnetion object isn't a part of state
      //{'connectionId'=>{iceConnectionState: 'state', clientId: 'aPeerId'}
    };
    this.webRtcConnections = {};
    this.peerViews = {};
    var self = this;
    window.onunload = function() {
      self.close(); //THIS IS VERY IMPORTANT FOR REFRESHES (not 100% sure why)
    }
  }
  render() {
    const {peers, upstreamConnectionId} = this.state;
    let upstreamConnectionState;
    const upstreamConnectionInfo = this.state.peers.get(upstreamConnectionId);
    if(upstreamConnectionInfo) {
      upstreamConnectionState = upstreamConnectionInfo.iceConnectionState;
    }
    return (
      <div className="App">
        <div className="App-header">
          <h2>WebRTC SFU Client Scaffold</h2>
        </div>
        <PeerConnector
          onConnect={this.onConnect.bind(this)}
          onMessage={this.onMessage.bind(this)}
          onLocalClientIdChange={this.setLocalClientId.bind(this)}
          onSessionIdChange={this.setSessionId.bind(this)}
        />
        <button onClick={this.getUserMedia.bind(this)}>Get User Media</button>
        <button onClick={this.sendOffer.bind(this)}>Send Offer</button>
        <button onClick={this.close.bind(this)}>Close</button>
        <div style={{display: 'flex', flexWrap: 'wrap', justifyContent: 'center', background: 'lightblue'}}>
          <PeerView
            ref={(peerView) => {this.peerViews[upstreamConnectionId] = peerView}}
            connectionId={upstreamConnectionId}
            clientId={this.state.localClientId || 'local client id not set yet'}
            connectionState={upstreamConnectionState || 'no state yet'}
            isLocal={true}
          />
          {Array.from(peers.keys()).map((connectionId, index) => {
            const {iceConnectionState, clientId} = peers.get(connectionId);
            if(connectionId === upstreamConnectionId) {
              return false; //local gets rendered separately so we can see video before connection
            }
            if(iceConnectionState === 'closed') {
              return false; //dont' render closed connections (caused by refresh likely)
              //keeping them in the instance vars for now but we could conceivably just
              //remove them there (and handle any npes for late messages)
              //reason: it seems like on refresh we need to use a new connectionId
              //in order to get offers for the existing peers
            }
            return (
              <PeerView
                key={connectionId}
                ref={(peerView) => {this.peerViews[connectionId] = peerView}}
                connectionId={connectionId}
                clientId={clientId}
                connectionState={iceConnectionState}
              />
              );
          })}
        </div>
      </div>
    );
  }
  setLocalClientId(localClientId) {
    this.setState({localClientId});
    //same as local client ID so that when we refresh we use the same connection
    //TODO - we should really persist a UUID for these so they are truly unique
    //although technically the peerID would be unique for a LiveExpert call
    this.setState({localDeviceId: localClientId});
    this.setState({localUserId: localClientId});
    this.setState({upstreamConnectionId: uuidV4()});
  }
  setSessionId(sessionId) {
    this.setState({sessionId});
  }
  getUserMedia() {
    const constraints = {
      audio: true,
      video: true
    };
    return navigator.mediaDevices.getUserMedia(constraints)
      .then(this.handleUserMediaSuccess.bind(this))
      .catch(this.handleUserMediaError.bind(this));
  }
  handleUserMediaSuccess(stream) {
    const videoTracks = stream.getVideoTracks();
    this.setState({videoDevice: videoTracks[0].label});
    stream.oninactive = () => {
      console.log('stream inactive');
    };
    this.peerViews[this.state.upstreamConnectionId].setSrcObject(stream);
    //this.localVideoEl.srcObject = stream;
    this.localStream = stream;
  }
  handleUserMediaError(error) {
    console.log('user media error: ' + JSON.stringify(error));
  }
  onConnect(sendMessage) {
    //TODO maybe these methods should already exist and throw exception or log error if we're not connected yet...
    this.sendMessageToSfu = (message) => {
      sendMessage('sfu', message);
    }

    //kinda hacky but...
    this.getUserMedia()
      .then(() => {
        this.sendOffer();
      })
      .catch((error) => {
        console.error('gum or offer error', error);
      });
  }
  sendOffer() {
    const {localClientId, upstreamConnectionId} = this.state;
    if(!localClientId) {
      window.alert('localClientId not set, cannot create offer');
      return;
    }
    this.createConnection(upstreamConnectionId);
    this.webRtcConnections[upstreamConnectionId].setLocalStream(this.localStream); //we only set local stream when we create offer (we're uploading)
    this.webRtcConnections[upstreamConnectionId].createOffer()
    //when we create an offer we don't have a relatedClientId, etc because we are uploading video
    .then((offer) => {
      const message = {
        applicationId,
        sessionId: this.state.sessionId,
        clientId: this.state.localClientId,
        connectionId: upstreamConnectionId,
        deviceId: this.state.localDeviceId,
        payload: JSON.stringify({sdpMessage: offer.sdp, isOffer: true}),
        type: 'offer',
        userId: this.state.localUserId
      };
      this.sendMessageToSfu(message);
    })
    .catch((error) => {
      console.error('error from create offer',error);
    });
  }
  createConnection(connectionId, relatedClientId, relatedDeviceId, relatedUserId) {
    if(this.webRtcConnections[connectionId]) {
      console.log('did not create new connetion for connectionId' + connectionId + ', one already exists');
    } else {
      const connection = new WebRtcConnection(connectionId,[{'url': 'stun:stun.l.google.com:19302'}]);
      connection.on('candidate',(candidate) => {
        const message = {
          applicationId,
          sessionId: this.state.sessionId,
          clientId: this.state.localClientId,
          connectionId: connectionId,
          deviceId: this.state.localDeviceId,
          userId: this.state.localUserId,
          payload: JSON.stringify({sdpCandidateAttribute: candidate.candidate, sdpMediaIndex: candidate.sdpMLineIndex}),
          type: 'candidate',
          relatedClientId, //this will be null/undefined for upstream connections
          relatedDeviceId, //this will be null/undefined for upstream connections
          relatedUserId //this will be null/undefined for upstream connections
        };
        this.sendMessageToSfu(message);
        //"{\"sdpCandidateAttribute\":\"a=candidate:2162125114 2 udp 2122260222 10.0.0.10 54249 typ host generation 0 ufrag nHYC network-id 1 network-cost 10\",\"sdpMediaIndex\":0}"
      });
      connection.on('streamAdded', (stream) => {
        console.log('stream added for connection ' + connectionId, stream);
        this.peerViews[connectionId].setSrcObject(stream);
      });
      connection.on('iceConnectionStateChange', (iceConnectionState) => {
        const {peers} = this.state;
        const peerInfo = peers.get(connectionId);
        peerInfo.iceConnectionState = iceConnectionState; //TODO is this safe or do we need to copy the map
        const newPeers = peers.set(connectionId, peerInfo);
        this.setState({peers: newPeers});
      });
      this.webRtcConnections[connectionId] = connection;

      //don't include ourself as a "peer"
      //TODO - clean this up, basing on the fact that relatedClientId only exists for downstream right now
      const {peers} = this.state;
      const newPeers = peers.set(connectionId, {iceConnectionState: 'initial state', clientId: relatedClientId});
      this.setState({peers: newPeers});
    }
  }
  closeConnection(connectionId) {
    const connection = this.webRtcConnections[connectionId];
    try {
      connection.close();
    } catch(e) {
      console.error('failed to close connection', e);
    }
    //maintain the closed connection and render a black box for now
    //delete this.webRtcConnections[connectionId];
    //const {peers} = this.state;
    //const remainingPeers = peers.delete(connectionId); //remove from UI, means we won't show a "closed" ice connection state
    //this.setState({peers: remainingPeers});
    //TODO - reconsider how this works. refreshes will create new downstream connections for the other peers,
    //which appear as black boxes currently
    //maybe we should get rid of the connections when they close OR only render 1 video view clientId

  }
  close() {
    console.log('clicked close button');
    Object.keys(this.webRtcConnections).forEach((connectionId) => {
      this.closeConnection(connectionId);
    });
    //send the close message to the sfu, which will tell the other clients to close their downstream of of our upstream connection
    const message = {
      type: 'close',
      applicationId,
      sessionId: this.state.sessionId,
      userId: this.state.localUserId,
      deviceId: this.state.localDeviceId,
      clientId: this.state.localClientId
    };
    this.sendMessageToSfu(message);
  }
  onMessage(message) {
    const {type, payload} = message; //type would be from the SFU (offer, answer, candidate, close?)
    //grab "const" connectionId here because it is block scoped (can't be repeated) and apparently
    //each case of a switch is not a new block
    //if we have other common message props we can define here rather than using message.whateverId everywhere
    const {connectionId} = message;
    switch(type) {
      case 'text':
        console.log('new text message', message);
        break;
      case 'offer':
        //TODO - test this, I dont' think we can just pass the payload in unaltered (it will be a string no?)
        const {relatedClientId, relatedUserId, relatedDeviceId} = message;
        console.log('received offer for related client Id: ' + relatedClientId, message);
        if(relatedClientId === this.state.localClientId) {
          console.error('received offer for our own clientid-- ignoring');
          //seen this when refreshing and sending a new offer. need to dig in
          return;
        }
        this.createConnection(connectionId, relatedClientId, relatedDeviceId, relatedUserId);
        //payload: "{"sdpMessage":"v=0\r\no=- 8439507244743020544 1241909014 IN IP4 127.0.0.1\r\ns=IceLink\r\nt=0 0\r\na=ice-options:trickle\r\nm=audio 9 UDP\/TLS\/RTP\/SAVPF 96 \r\nc=IN IP4 0.0.0.0\r\na=ice-ufrag:e50488bf\r\na=ice-pwd:f7aaa4cbab19ecffbefb74fe531e0b8d\r\na=fingerprint:sha-256 2A:34:57:82:1D:85:AD:46:58:DE:04:E4:18:FB:3E:29:D5:D6:E0:BA:21:E6:4A:D5:A3:ED:9E:31:AE:49:92:DF\r\na=setup:actpass\r\na=rtcp-mux\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=sendonly\r\na=rtpmap:96 opus\/48000\/2\r\nm=video 9 UDP\/TLS\/RTP\/SAVPF 97 \r\nc=IN IP4 0.0.0.0\r\na=ice-ufrag:5b974c35\r\na=ice-pwd:b56940ee7b175956c25d9f1ed9ca8953\r\na=fingerprint:sha-256 2A:34:57:82:1D:85:AD:46:58:DE:04:E4:18:FB:3E:29:D5:D6:E0:BA:21:E6:4A:D5:A3:ED:9E:31:AE:49:92:DF\r\na=setup:actpass\r\na=rtcp-mux\r\na=rtcp:9 IN IP4 0.0.0.0\r\na=sendonly\r\na=rtpmap:97 VP8\/90000\r\n","isOffer":true}"
        const offer = JSON.parse(payload);
        this.webRtcConnections[connectionId].processOffer({sdp: offer.sdpMessage, type: 'offer'})
        .then((answer) => {
          const message = {
            applicationId,
            sessionId: this.state.sessionId,
            clientId: this.state.localClientId,
            connectionId, //this came from the message
            deviceId: this.state.localDeviceId,
            userId: this.state.localUserId,
            payload: JSON.stringify({sdpMessage: answer.sdp, isOffer: false}),
            type: 'answer',
            //TODO are the related necessary? - they mean its a downstream from another peer
            relatedClientId,
            relatedDeviceId,
            relatedUserId
          };
          this.sendMessageToSfu(message);
        })
        .catch((error) => {
          console.error('error processing offer', error);
        });
        break;
      case 'answer':
        console.log('processing answer', message);
        const json = JSON.parse(payload);
        this.webRtcConnections[connectionId].processAnswer({sdp: json.sdpMessage, type: 'answer'})
        .catch((error) => {
          console.log('process answer error...', error);
          console.log('message was', message);
        });
        break;
      case 'candidate':
        console.log('processing candidate', message);
        const cand = JSON.parse(payload);
        this.webRtcConnections[connectionId].processCandidate({sdpMLineIndex: cand.sdpMediaIndex, candidate: cand.sdpCandidateAttribute});
        break;
      case 'close':
        console.log('got close message for connection: ' + connectionId);
        this.closeConnection(connectionId);
        break;
      default:
        console.warn('unkown message type: ' + type, message);
    }
  }
}
export default App;
