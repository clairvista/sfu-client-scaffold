import React, {Component, PropTypes} from 'react';
import '../node_modules/stompjs/lib/stomp';

class PeerConnector extends Component {
  constructor(props) {
    super(props);
    this.state = {
      localClientId: '',
      sessionId: ''
    };
  }
  render() {
    const {localClientId, sessionId} = this.state;
    return (
      <div>
        <input type='text' value={localClientId} placeholder='local client Id' onChange={this.setLocalClientId.bind(this)} />
        <input type='text' value={sessionId} placeholder='session Id' onChange={this.setSessionId.bind(this)} />
        <button onClick={this.onSubscribeClicked.bind(this)}>Subscribe</button>
      </div>
    )
  }
  setLocalClientId(event) {
    this.setState({localClientId: event.target.value});
  }
  setSessionId(event) {
    const sessionId = event.target.value;
    this.setState({sessionId});
    this.updateWindowHash({sessionId});
    this.props.onSessionIdChange(sessionId);
  }
  onSubscribeClicked() {
    const {localClientId} = this.state;
    this.subscribe(localClientId);
  }
  subscribe(localClientId) {
    console.log('subscribe');
    if(!localClientId.trim()) {
      alert('no local peer id to subscribe to messages for');
      return;
    }
    if(this.subscription) {
      console.log('unsubscribing first...')
      this.subscription.unsubscribe();
    }
    this.subscription = this.client.subscribe('/queue/' + this.state.localClientId, this.onMessage.bind(this));
    this.updateWindowHash({localClientId});
    this.props.onLocalClientIdChange(localClientId);
  }
  updateWindowHash(newStuff) {
    const currentHash = window.location.hash;
    if(!currentHash) {
      window.location.hash = JSON.stringify(newStuff);
      return;
    }
    const currentObj = JSON.parse(currentHash.substring(1));
    const newObj = Object.assign({}, currentObj, newStuff);
    window.location.hash = JSON.stringify(newObj);
  }
  componentDidMount() {
    //IMPORTANT - since this component "owns" the client, it needs to always be mounted, we could just render "false"
    //if we don't want it to show up
    //
    this.client = window.Stomp.client('ws://localhost:61614');
    //client has a debug fn we can define
    this.client.debug = function(str) {
      //console.log('client debug: ' + str);
    }
    //uname guest, password guest
    this.client.connect('guest', 'guest', this.onConnect.bind(this));

    const locationHash = window.location.hash;
    if(locationHash) {
      const rememberedIds = JSON.parse(locationHash.substring(1)); //remove the leading "#"
      const {localClientId, sessionId} = rememberedIds;
      this.setState({localClientId, sessionId});

      //notify parent of current peerIds
      this.props.onLocalClientIdChange(localClientId);
      this.props.onSessionIdChange(sessionId);
    }
  }
  onConnect(frame) {
    console.log('ws connected (you still need to subscribe), frame is', frame); //what is in the frame?
    this.props.onConnect(this.sendMessage.bind(this));

    const {localClientId} = this.state;
    if(localClientId) {
      //auto subscribe since we know the peer ids
      this.subscribe(localClientId); //subscribe fn wouldn't have access to state at this point it seems
    }
  }
  componentWillUnmount() {
    //should this be automatic?
    if(this.subscription) {
      this.subscription.unsubscribe();
    }
  }
  onMessage(message) {
    const {body} = message;
    //TODO make sure body is always text of a JSON (ie starts with a quote)
    this.props.onMessage(JSON.parse(body));
  }
  sendMessage(peerId, message) {
    //this would need to be refactored if we have multiple peers
    console.log('sending ' + message.type, message);
    this.client.send('/queue/' + peerId, null /*header*/, JSON.stringify(message));
  }
}

PeerConnector.propTypes = {
  onConnect: PropTypes.func.isRequired,
  onMessage: PropTypes.func.isRequired,
  onSessionIdChange: PropTypes.func.isRequired
}

export default PeerConnector;
